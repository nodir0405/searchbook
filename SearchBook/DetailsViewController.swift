//
//  DetailsViewController.swift
//  SearchBook
//
//  Created by Jurayev Nodir on 24/05/21.
//

import UIKit

class DetailsViewController: UIViewController {

    let scrollView = UIScrollView()
    let imageView = UIImageView()
    let nameLabel = UILabel()
    let authorLabel = UILabel()
    let descLabel = UILabel()
    
    
    let wrapperView = UIView()
    
    var book : Book!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(scrollView)
        scrollView.addSubview(imageView)
        scrollView.addSubview(nameLabel)
        scrollView.addSubview(authorLabel)
        scrollView.addSubview(descLabel)
        scrollView.addSubview(wrapperView)
        
        scrollView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
        }
        wrapperView.backgroundColor = .clear
        wrapperView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.centerX.centerY.equalToSuperview()
            make.width.equalToSuperview()

        }
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .red
        imageView.snp.makeConstraints { (make) in
            make.height.equalTo(208)
            make.width.equalTo(128)
            make.centerX.equalToSuperview()
            make.top.equalTo(10)
            
        }
        nameLabel.numberOfLines = 3
        nameLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 14.0)
        
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.top.equalTo(imageView.snp.bottom).offset(15)
        }
        
        authorLabel.numberOfLines = 3
        authorLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 12.0)
        authorLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.top.equalTo(nameLabel.snp.bottom).offset(15)
        }
        
        descLabel.numberOfLines = 100
        descLabel.font = UIFont(name: "HelveticaNeue", size: 12.0)
        descLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.top.equalTo(authorLabel.snp.bottom).offset(15)
        }
        
        imageView.sd_setImage(with: URL(string: book.thumbnail ?? "")) { (image, error, type, url) in
            
        }
        nameLabel.text = book?.title
        if let arr = book?.authors{
            authorLabel.text = arr.joined(separator: " ")
        }
        descLabel.text = book.desc
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
}
