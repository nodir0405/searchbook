//
//  ViewController.swift
//  SearchBook
//
//  Created by Jurayev Nodir on 23/05/21.
//

import UIKit

class ViewController: UIViewController {
    let tableView = UITableView()
    
    private var searchController: UISearchController? = UISearchController(searchResultsController: nil)
    
    var results = [Book]()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Search book"
        self.tableView.register(BookListTableViewCell.self, forCellReuseIdentifier: "BookListTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchController?.searchBar.tintColor = UIColor.lightGray
        self.searchController?.searchBar.barStyle = .default
        self.searchController?.searchBar.barTintColor = UIColor.gray
        self.searchController?.searchBar.isTranslucent = true
        self.initSearchBarController()
        
        view.addSubview(tableView)
        tableView.tableFooterView = UIView()
        tableView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.allowsMultipleSelection = false
    }
    
    func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func initSearchBarController() {
        self.searchController?.searchResultsUpdater = self
        self.searchController?.delegate = self
        self.searchController?.obscuresBackgroundDuringPresentation = false
        
        if #available(iOS 11.0, *) {
            /* default */
            self.navigationItem.searchController = self.searchController
        } else {
            self.searchController?.dimsBackgroundDuringPresentation = false
            self.searchController?.hidesNavigationBarDuringPresentation = true
            self.searchController?.definesPresentationContext = true
            self.tableView.tableHeaderView = self.searchController?.searchBar
        }
        self.searchController?.searchBar.sizeToFit()
        self.searchController?.searchBar.tintColor = UIColor.lightGray
        self.searchController?.searchBar.placeholder = "Search"
        self.definesPresentationContext = true
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    @objc func searchBooks(_ searchText:String){
        let endpoint = BookEndpoint.search(searchText)
        ApiManager.request(endpoint: endpoint, completion: { (status: Int?, data:Any?, error:String?) in
            if let result = data as? [ String : Any]{
                let items = result["items"] as? [[String : Any]]
                self.results.removeAll()
                for obj in items ?? [] {
                    let book = Book(json: obj)
                    self.results.append(book)
                }
            } else {
                
                
            }
            self.tableView.reloadData()
        })
    }
    @objc func changeSearchText(_ text:String){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(searchBooks(_:)), object: text)
        self.perform(#selector(searchBooks(_:)), with: text, afterDelay: 0.3)
    }
    func openDetails(_ book:Book){
        let controller = DetailsViewController()
        controller.book = book
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookListTableViewCell", for: indexPath) as! BookListTableViewCell
        cell.setupBook(results[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        openDetails(results[indexPath.row])
    }
    
    
   
}

extension ViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text == "" {
            self.results.removeAll()
            self.tableView.reloadData()
            return
        }
        changeSearchText(searchController.searchBar.text!)
        
    }
}

extension ViewController: UISearchControllerDelegate {
    func didPresentSearchController(_ searchController: UISearchController) {
        DispatchQueue.main.async { [unowned self] in
            self.searchController?.searchBar.becomeFirstResponder()
            (self.searchController?.searchBar.value(forKey: "cancelButton") as! UIButton).setTitle("Cancel", for: .normal)
        }
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
//        self.results.removeAll()
//        self.tableView.reloadData()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        self.dismissController()
    }
}
