//
//  BookEndpoint.swift
//  SearchBook
//
//  Created by Jurayev Nodir on 24/05/21.
//

import UIKit
import Alamofire
enum BookEndpoint : Endpoint {
    
    case search(_ search:String)
    var path: String {
        switch self {
        case .search:
            return "/volumes"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .search:
            return .get
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .search(let search):
            return ["q":search]
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .search:
            return URLEncoding.queryString //JSONEncoding.default
        }
    }
    
    
}
