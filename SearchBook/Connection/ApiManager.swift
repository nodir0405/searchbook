//
//  ApiManager.swift
//  SearchBook
//
//  Created by Jurayev Nodir on 23/05/21.
//

import UIKit
import Alamofire

var apiUrl : String{
    return "https://www.googleapis.com/books/v1"
}

protocol Endpoint : URLRequestConvertible, CustomStringConvertible {
    var path:String { get }
    var method:HTTPMethod { get }
    var parameters:Parameters { get }
    var timeout:Double { get }
    var encoding:ParameterEncoding { get }
    var cacheable:Bool { get }
}

extension Endpoint {
    
    public var urlString:URL {
        var url = try! apiUrl.asURL()
        url = url.appendingPathComponent(self.path)
        return url
    }
    
    public func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: self.urlString)
        urlRequest.httpMethod = self.method.rawValue
        urlRequest.allHTTPHeaderFields = self.headers
        urlRequest.timeoutInterval = self.timeout
        urlRequest = try self.encoding.encode(urlRequest, with: self.parameters)
        return urlRequest
    }
 
    public var headers:[String:String] {
        var data = [String:String]()
        data["app-version"] = "1.0"
        return data
    }
    
    var description: String {
        var string = ""
        string += "url: \(self.urlString)\n"
        string += "method: \(self.method)\n"
        string += "headers: \(self.headers)\n"
        string += "parameters: \(self.parameters)\n"
        string += "timeout: \(self.timeout)"
        return string
    }
    
    var timeout: Double {
        return 60
    }
    
    var cacheable: Bool {
        return true
    }
    
}

class ApiManager: NSObject {
    
   static func request(endpoint:Endpoint,
                       completion: @escaping (_ status:Int?, _ response:Any?, _ error:String?) -> Void) {
        AF.request(endpoint).responseJSON { (response) in
            if let httpResponse = response.response {
                let statusCode = httpResponse.statusCode
                completion(statusCode, response.value, nil)
            } else if let error = response.error {
                completion(nil, nil, error.underlyingError?.localizedDescription)
            }
        }
    }
    
}
