//
//  BookListTableViewCell.swift
//  SearchBook
//
//  Created by Jurayev Nodir on 23/05/21.
//

import UIKit
import SnapKit
import SDWebImage

class BookListTableViewCell: UITableViewCell {

    let thumbnailView = UIImageView()
    let titleLabel = UILabel()
    let authorLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(thumbnailView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(authorLabel)
        
        thumbnailView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.top.equalTo(5)
            make.bottom.equalTo(-5)
            make.height.width.equalTo(80)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(thumbnailView)
            make.right.equalTo(-5)
            make.left.equalTo(thumbnailView.snp.right).offset(10)
        }
        titleLabel.numberOfLines = 3
        titleLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 14.0)
        
        authorLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(thumbnailView)
            make.left.equalTo(titleLabel)
            make.right.equalTo(titleLabel)
            make.height.equalTo(22)
        }
        authorLabel.font = UIFont(name: "HelveticaNeue", size: 12.0)
        
    }
    func setupBook(_ book:Book?){
        thumbnailView.sd_setImage(with: URL(string: book?.thumbnail ?? "")) { (image, error, type, url) in
            //print(image)
        }
        titleLabel.text = book?.title
        if let arr = book?.authors{
            authorLabel.text = arr.joined(separator: " ")
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
