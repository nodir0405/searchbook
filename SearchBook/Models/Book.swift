//
//  Book.swift
//  SearchBook
//
//  Created by Jurayev Nodir on 23/05/21.
//

import UIKit
//thumbnail, title and author of the book.
class Book {
    var authors : [String]?
    var name: String?
    var desc : String?
    var thumbnail : String?
    var title:String?
    var jsonData:[String:Any]
    
    init(json:[String : Any]) {
        
        let volumeInfo = json["volumeInfo"] as? [String : Any]
        self.authors = volumeInfo?["authors"] as? [String]
        let links = volumeInfo?["imageLinks"] as? [String : String]
        self.thumbnail = links?["thumbnail"]
        self.title = volumeInfo?["title"] as? String
        self.desc = volumeInfo?["description"] as? String
        self.jsonData = json
    }
}
